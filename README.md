# Title: "Accurate long-read sequencing identified GBA variants as a major risk factor for parkinsonism in the Luxembourgish cohort"
### Author: "Sinthuja Pachchek Peiris"

# tools version
```bash
ANNOVAR=2020-06-08 00:46:07 -0400 (Mon,  8 Jun 2020)
TABIX=1.9
BCFTOOLS=1.9
VCFTOOLS=0.1.16
PLINK=v1.90b6.18 64-bit (16 Jun 2020)
```

# For the NeuroChip array dataset
(1) convert plink file to VCF
```bash
plink --bfile input_plink_file --recode vcf-iid --output-chr chrM --out output_plink_file --allow-no-sex 
```

(2) QC steps : exclude low genotyping rate (<95%) and Hardy-Weinberg equilibrium P-value < 1x10-6
```bash
script/qc.sh vcf_input
```

(3) As sometimes the NeuroChip does not have the correct REF allele, we need to proceed with a flip script to convert and flip the correct REF and ALT alleles
```bash
script/flip.sh input_vcf hg19
```

(4) Merge QCed VCFs from different NeuroChip batches
```bash
bcftools merge --threads 5 --merge none NeuroChip_batch1.QCed.flip.vcf.gz NeuroChip_batch2.QCed.flip.vcf.gz NeuroChip_batch3.QCed.flip.vcf.gz --output LuxPARK.QCed.flip.merge.vcf
```

(5) Variant annotation with ANNOVAR
```bash
script/Annovar.Annotation.sh input_vcf hg19 basic
```

(6) Convert VCF to tsv
```bash
script/convertVCF2TSV.sh input_vcf hg19 basic
```

(7) Counts the number of samples having the variant with zygotie information and separated by category of diseases
```bash
script/SamplesCount_by_variants_NeuroChip.sh input_tsv
```

(8) select rare variants from PD causales genes
	#) selected variant by genes
```bash
python script/select_variant_by_genes.py input_tsv PD_genes.list output_tsv
```
	#) selected rare variants with MAF lower than 0,01
```bash
python script/rare_variants.py input_tsv 0.01
```

# For WGS dataset from fastq files
(1) call the variants we used the Illumina’s Dragen DNA pipeline from the Bio-IT Illumina Dynamic Read Analysis for GENomics (DRAGEN; v3.8; Illumina) platform
```bash
ref_dir=/staging/reference/human/GRCh38/Homo_sapiens.GRCh38.dna.primary_assembly.fa.k_21.f_16.m_149
repeat_genotype=/opt/edico/repeat-specs/grch38

dragen -f -r $ref_dir -1 fastq1 -2 fastq2 --output-directory output_directory --output-file-prefix sample_name --RGID sample_name --RGSM sample_name --enable-map-align-output true --enable-bam-indexing true --enable-duplicate-marking true --enable-variant-caller true --intermediate-results-dir temp --enable-cnv true --cnv-enable-self-normalization true --enable-sv true --cnv-segmentation-mode SLM --cnv-enable-tracks true --repeat-genotype-enable true --repeat-genotype-specs $repeat_genotype --vc-enable-gatk-acceleration true

```

(2) QC steps
```bash
script/qc_WGS.sh input_vcf ref(path to GRCh37.fa or ucsc.hg19.fasta)
```

(3) Merge QCed VCFs from different NeuroChip batches
```bash
bcftools merge -m none --file-list input_samples.list -Oz -o LuxPARK.WGS.QCed.vcf.gz
```

(4) Variant annotation with ANNOVAR
```bash
script/Annovar.Annotation.sh input_vcf hg19 basic
```

(5) Convert vcf to tsv
```bash
script/convertVCF2TSV.sh input_vcf hg19 basic
```

(6) Counting of samples having the variants with the sample_ID and zygotie informations and separated by category of diseases
```bash
script/SamplesCount_by_variants.sh input_tsv
```

(7) select rare variants from PD causales genes
	#) selected variant by genes
```bash
python script/select_variant_by_genes.py input_tsv PD_genes.list output_tsv
```
	#) selected rare variants with MAF lower than 0,01
```bash
python script/rare_variants.py input_tsv 0.01
```

# For GBA-targeted PacBio dataset from VCF
(1) QC steps
	# selected the variant with quality above 30 (QUAL>30)
```bash
bcftools filter -s LowQual -e '%QUAL<30' > LuxPARK.PacBio.QUAL30.vcf.gz
```
	#)Removes all sites with a FILTER flag other than PASS
```bash
vcftools --gzvcf LuxPARK.PacBio.QUAL30.vcf.gz --recode --recode-INFO-all --remove-filtered-all --out LuxPARK.PacBio.QCed
```

(2) Merge QCed VCFs
```bash
bcftools merge -m none --file-list input_samples.list -Oz -o LuxPARK.PacBio.QCed.merge.vcf.gz
```

(4) Variant annotation with ANNOVAR
```bash
script/Annovar.Annotation.sh input_vcf hg19 basic

```

(5) Convert vcf to tsv
```bash
script/convertVCF2TSV.sh input_vcf hg19 basic
```

(6) Counting of samples having the variants with the sample_ID and zygotie informations and separated by category of diseases
```bash
script/SamplesCount_by_variants.sh input_tsv
```

# Statistical analysis
	# Regression models (linear and logistic) to assess the association of clinical features with the GBA variant carrier status in PD patients
	## adjusted : Gender + Age_at_Basic_Assessment + Disease_duration
	## target must be binary value : GBA carriers (1 or 0)
```bash
python script/association_study.py input.tsv continious_outcome.list binary_outcome.list target_column
```

# Structural Analysis of GBA Gene

(1) PDB structure accession code 1ogs.pdb

(2) Action remove water

(3) manually remove ligands associated with the protein, SO4 ions and NAG

(4) color domains
```bash
	select Domain_I, resi 1-27 + resi 383-414
	color yellow, Domain_I
	select Domain_II, resi 30-75 + resi 431-497
	color cyan, Domain_II
	select Domain_III, resi 76-381 + resi 416-430
	color pink, Domain_III
```

(5) select active residus
```bash
	select active_residus,resi 235+340
	show sticks, active_residus
	color red, active_residus
```

(6) hide chaine B
```bash
	select chainB, chain B
	hide (chainB)
```

(7) mapp VUS
```bash
	select VUS,A/22+39+58+174+176+388+395+456+490+495/CA
	show spheres, VUS
	color purple, VUS
```

(8) mapp severe variants
```bash
	select severe,A/15+23+48+78+79+100+113+120+122+125+131+134+137+157+170+182+184+188+191+193+195+196+197+198+201+202+205+213+231+250+251+255+257+259+260+264+266+271+285+289+304+309+311+312+315+324+325+336+342+345+352+353+354+356+364+366+371+374+377+378+380+382+385+386+387+390+391+394+401+409+415+427+444+447+448+460+462+463+478+482/CA
	show spheres, severe
	color red, severe
```

(9) mapp mild variants
```bash
	select mild,A/105+123+135+140+216+248+277+323+329+370+375+393+396+398+399+411+446+496/CA
	show spheres, mild
	color orange, mild
```

(10) mapp low-risk variants
```bash
	select lowrisk,A/326+368+369+441+443/CA
	show spheres, lowrisk
	color yellow, lowrisk
```

(11) label VUS residus in one latter
```bash
	one_letter ={'VAL':'V', 'ILE':'I', 'LEU':'L', 'GLU':'E', 'GLN':'Q','ASP':'D', 'ASN':'N', 'HIS':'H', 'TRP':'W', 'PHE':'F', 'TYR':'Y', 'ARG':'R', 'LYS':'K', 'SER':'S', 'THR':'T', 'MET':'M', 'ALA':'A', 'GLY':'G', 'PRO':'P', 'CYS':'C'}
	label VUS, "%s%s" % (one_letter[resn], resi)
	set label_size, 25
```

(12) set white background
```bash
	bg_color white 
```

(13) manually convert legacy residus name to HGVS name




