#!/home/users/spachchek/miniconda3/bin/python
######################################################
#########  AUTHOR : Sinthuja PACHCHEK PEIRIS #########
######################################################

# python ./SamplesCount_by_variants.py $input_tsv Final.$input_tsv $FORMAT $cases $controls $no_pheno $control_inf_age_50 $control_sup_age_50 $cases_alphasyn $cases_tau_pathies

import sys
import pandas
import csv
import numpy
import re
from numpy import nan
from numpy import isnan
import os

input_tsv= sys.argv[1]
output = sys.argv[2]
FORMAT = sys.argv[3]
cases = sys.argv[4]
controls = sys.argv[5]
unspecified_disease = sys.argv[6]
control_inf_age_50 = sys.argv[7]
control_sup_age_50 = sys.argv[8]
cases_alphasyn = sys.argv[9]
cases_tau_pathies = sys.argv[10]
cases_Secondary_parkinsonism_vascular_parkinsonism = sys.argv[11]
no_pheno = sys.argv[12]

def annotate_genotype_info2(input_tsv, output, FORMAT):

	### created a dict datafram with pandas to store all variants
	data = pandas.read_csv(input_tsv, delimiter='\t', dtype={'CHR': 'str'}, na_values=['.', 'NA', 'nan', '.:.:.' , '.:.:.:.', './.' , '.|.'], low_memory=False)
	data = data.astype(str)


	lenght = len(data.columns)
	i = int(FORMAT)
	print(data.columns[i-1])
	print(data.columns[i])

	while i < lenght :
		colname = data.columns[i]
		data[colname] = data[colname].astype(str)
		#colname_split = colname.split("_")[0]
		colname_split = colname

		data[[colname]] = data[[colname]].replace(to_replace=['^0/1.*$', '^1/0.*$', '^\./1.*$', '^1/\..*$'],value=[colname_split+'(het)', colname_split+'(het)', colname_split+'(het)', colname_split+'(het)'], regex=True)
		data[[colname]] = data[[colname]].replace(to_replace=['^0\|1.*$', '^1\|0.*$', '^\.\|1.*$', '^1\|\..*$'],value=[colname_split+'(het)', colname_split+'(het)', colname_split+'(het)', colname_split+'(het)'], regex=True)
		data[[colname]] = data[[colname]].replace(to_replace=['^1/1.*$', '^1\|1.*$'],value=[colname_split+'(hom)', colname_split+'(hom)'], regex=True)
		data[[colname]] = data[[colname]].replace(to_replace=['^0/0.*$', '^0\|0.*$', '^0/\..*$', '^0\|\..*$', '^\./0.*$', '^\.\|0', '^\./\.', '^\.\|\.'],value=[None, None, None, None, None, None, None, None], regex=True)

		### specific for chrom X or Y
		# data[[colname]] = data[[colname]].replace(to_replace=['^\.:.*$'],value=[None], regex=True)
		# data[[colname]] = data[[colname]].replace(to_replace=r'^\.:*$', value=None, regex=True)
		# data[[colname]] = data[[colname]].replace(to_replace=['^1:.*$'],value=[colname_split+'(1)'], regex=True)
		# data[[colname]] = data[[colname]].replace(to_replace=['^0:.*$'],value=[None], regex=True)

		i += 1
	
	data.to_csv(output + ".tmp", index=False, sep='\t')

	print ("end function annotate_genotype_info2")



def SamplesAaffected_and_SamplesCount_by_variants(input_tsv, output, FORMAT, cases, controls, unspecified_disease, control_inf_age_50, control_sup_age_50, cases_alphasyn, cases_tau_pathies, cases_Secondary_parkinsonism_vascular_parkinsonism, no_pheno):
	### created a dict datafram with pandas to store all variants
	data = pandas.read_csv(input_tsv, delimiter='\t', dtype={'CHR': 'str'}, na_values=['.', 'NA', 'nan', '.:.:.' , '.:.:.:.', './.' , '.|.'], low_memory=False)
	lenght = len(data.columns)
	i = int(FORMAT)

	print(data.columns[i-1])
	print(data.columns[i])

	### for all samples
	samples_liste_array=[]
	samples_count_array=[]
	samples_array_samples_het=[]
	samples_count_array_het=[]
	samples_array_samples_hom=[]
	samples_count_array_hom=[]


	#######################
	######## CASES ########
	#######################
	### for cases sample
	cases_samples_liste_array=[]
	cases_samples_count_array=[]
	cases_samples_liste_array_het=[]
	cases_samples_count_array_het=[]
	cases_samples_liste_array_hom=[]
	cases_samples_count_array_hom=[]

	### for cases_alphasyn sample
	cases_alphasyn_samples_liste_array=[]
	cases_alphasyn_samples_count_array=[]
	cases_alphasyn_samples_liste_array_het=[]
	cases_alphasyn_samples_count_array_het=[]
	cases_alphasyn_samples_liste_array_hom=[]
	cases_alphasyn_samples_count_array_hom=[]

	### for cases_tau_pathies sample
	cases_tau_pathies_samples_liste_array=[]
	cases_tau_pathies_samples_count_array=[]
	cases_tau_pathies_samples_liste_array_het=[]
	cases_tau_pathies_samples_count_array_het=[]
	cases_tau_pathies_samples_liste_array_hom=[]
	cases_tau_pathies_samples_count_array_hom=[]

	### for cases_Secondary_parkinsonism_vascular_parkinsonism sample
	cases_Secondary_parkinsonism_vascular_parkinsonism_samples_liste_array=[]
	cases_Secondary_parkinsonism_vascular_parkinsonism_samples_count_array=[]
	cases_Secondary_parkinsonism_vascular_parkinsonism_samples_liste_array_het=[]
	cases_Secondary_parkinsonism_vascular_parkinsonism_samples_count_array_het=[]
	cases_Secondary_parkinsonism_vascular_parkinsonism_samples_liste_array_hom=[]
	cases_Secondary_parkinsonism_vascular_parkinsonism_samples_count_array_hom=[]


	##########################
	######## CONTROLS ########
	##########################
	### for control sample
	control_samples_liste_array=[]
	control_samples_count_array=[]
	control_samples_liste_array_het=[]
	control_samples_count_array_het=[]
	control_samples_liste_array_hom=[]
	control_samples_count_array_hom=[]

	### for control_inf_age_50 sample
	control_inf_age_50_samples_liste_array=[]
	control_inf_age_50_samples_count_array=[]
	control_inf_age_50_samples_liste_array_het=[]
	control_inf_age_50_samples_count_array_het=[]
	control_inf_age_50_samples_liste_array_hom=[]
	control_inf_age_50_samples_count_array_hom=[]

	### for control_sup_age_50 sample
	control_sup_age_50_samples_liste_array=[]
	control_sup_age_50_samples_count_array=[]
	control_sup_age_50_samples_liste_array_het=[]
	control_sup_age_50_samples_count_array_het=[]
	control_sup_age_50_samples_liste_array_hom=[]
	control_sup_age_50_samples_count_array_hom=[]


	#####################################
	######## unspecified_disease ########
	#####################################		
	unspecified_disease_samples_liste_array=[]
	unspecified_disease_samples_count_array=[]
	unspecified_disease_samples_liste_array_het=[]
	unspecified_disease_samples_count_array_het=[]
	unspecified_disease_samples_liste_array_hom=[]
	unspecified_disease_samples_count_array_hom=[]


	##########################
	######## NO PHENO ########
	##########################		
	nopheno_samples_liste_array=[]
	nopheno_samples_count_array=[]
	nopheno_samples_liste_array_het=[]
	nopheno_samples_count_array_het=[]
	nopheno_samples_liste_array_hom=[]
	nopheno_samples_count_array_hom=[]





	#######################
	######## CASES ########
	#######################
	### for cases sample
	cases_array_samples = []
	with open(cases, 'r') as cases_file:
		for line in cases_file:
			sample=line.strip()
			cases_array_samples.append(sample)

	### for cases_alphasyn sample
	cases_alphasyn_array_samples = []
	with open(cases_alphasyn, 'r') as cases_alphasyn_file:
		for line in cases_alphasyn_file:
			cases_alphasyn_sample=line.strip()
			cases_alphasyn_array_samples.append(cases_alphasyn_sample)

	### for cases_tau_pathies sample
	cases_tau_pathies_array_samples = []
	with open(cases_tau_pathies, 'r') as cases_tau_pathies_file:
		for line in cases_tau_pathies_file:
			cases_tau_pathies_sample=line.strip()
			cases_tau_pathies_array_samples.append(cases_tau_pathies_sample)

	### for cases_Secondary_parkinsonism_vascular_parkinsonism sample
	cases_Secondary_parkinsonism_vascular_parkinsonism_array_samples = []
	with open(cases_Secondary_parkinsonism_vascular_parkinsonism, 'r') as cases_Secondary_parkinsonism_vascular_parkinsonism_file:
		for line in cases_Secondary_parkinsonism_vascular_parkinsonism_file:
			cases_Secondary_parkinsonism_vascular_parkinsonism_sample=line.strip()
			cases_Secondary_parkinsonism_vascular_parkinsonism_array_samples.append(cases_Secondary_parkinsonism_vascular_parkinsonism_sample)


	##########################
	######## CONTROLS ########
	##########################
	### for control sample
	control_array_samples = []
	with open(controls, 'r') as controls_file:
		for line in controls_file:
			sample2=line.strip()
			control_array_samples.append(sample2)

	### for control_inf_age_50 sample
	control_inf_age_50_array_samples = []
	with open(control_inf_age_50, 'r') as control_inf_age_50_file:
		for line in control_inf_age_50_file:
			control_inf_age_50_sample=line.strip()
			control_inf_age_50_array_samples.append(control_inf_age_50_sample)

	### for control_sup_age_50 sample
	control_sup_age_50_array_samples = []
	with open(control_sup_age_50, 'r') as control_sup_age_50_file:
		for line in control_sup_age_50_file:
			control_sup_age_50_sample=line.strip()
			control_sup_age_50_array_samples.append(control_sup_age_50_sample)

	#####################################
	######## unspecified_disease ########
	#####################################	
	unspecified_disease_array_samples = []
	with open(unspecified_disease, 'r') as unspecified_disease_file:
		for line in unspecified_disease_file:
			unspecified_disease_sample=line.strip()
			unspecified_disease_array_samples.append(unspecified_disease_sample)


	##########################
	######## NO PHENO ########
	##########################	
	### for no pheno sample
	nopheno_array_samples = []
	with open(no_pheno, 'r') as nopheno_file:
		for line in nopheno_file:
			sample3=line.strip()
			nopheno_array_samples.append(sample3)



	for index, row in data.iterrows():
		array_samples = list()
		array_samples_het = list()
		array_samples_hom = list()

		#######################
		######## CASES ########
		#######################
		### for cases sample
		cases_list_samples = list()
		cases_list_samples_het = list()
		cases_list_samples_hom = list()

		### for cases_alphasyn sample
		cases_alphasyn_list_samples = list()
		cases_alphasyn_list_samples_het = list()
		cases_alphasyn_list_samples_hom = list()

		### for cases_tau_pathies sample
		cases_tau_pathies_list_samples = list()
		cases_tau_pathies_list_samples_het = list()
		cases_tau_pathies_list_samples_hom = list()

		### for cases_Secondary_parkinsonism_vascular_parkinsonism sample
		cases_Secondary_parkinsonism_vascular_parkinsonism_list_samples = list()
		cases_Secondary_parkinsonism_vascular_parkinsonism_list_samples_het = list()
		cases_Secondary_parkinsonism_vascular_parkinsonism_list_samples_hom = list()


		##########################
		######## CONTROLS ########
		##########################
		### for control sample
		control_list_samples = list()
		control_list_samples_het = list()
		control_list_samples_hom = list()

		### for control_inf_age_50 sample
		control_inf_age_50_list_samples = list()
		control_inf_age_50_list_samples_het = list()
		control_inf_age_50_list_samples_hom = list()

		### for control_sup_age_50 sample
		control_sup_age_50_list_samples = list()
		control_sup_age_50_list_samples_het = list()
		control_sup_age_50_list_samples_hom = list()


		#####################################
		######## unspecified_disease ########
		#####################################
		unspecified_disease_list_samples = list()
		unspecified_disease_list_samples_het = list()
		unspecified_disease_list_samples_hom = list()


		##########################
		######## NO PHENO ########
		##########################	
		nopheno_list_samples = list()
		nopheno_list_samples_het = list()
		nopheno_list_samples_hom = list()






		### /!\ ### starting samples genotype
		i = int(FORMAT)
		while i < lenght :
			colname = data.columns[i]

			### for all samples
			array_samples.append(row[colname])
			val = str(row[colname])
			het="het"
			hom="hom"
			if het in val:
				array_samples_het.append(row[colname])
			elif hom in val:
				array_samples_hom.append(row[colname])

			#######################
			######## CASES ########
			#######################
			### for cases sample
			if colname in cases_array_samples :
				cases_list_samples.append(row[colname])
				if het in val:
					cases_list_samples_het.append(row[colname])
				elif hom in val:
					cases_list_samples_hom.append(row[colname])

			### for cases_alphasyn sample
			if colname in cases_alphasyn_array_samples :
				cases_alphasyn_list_samples.append(row[colname])
				if het in val:
					cases_alphasyn_list_samples_het.append(row[colname])
				elif hom in val:
					cases_alphasyn_list_samples_hom.append(row[colname])

			### for cases_tau_pathies sample
			if colname in cases_tau_pathies_array_samples :
				cases_tau_pathies_list_samples.append(row[colname])
				if het in val:
					cases_tau_pathies_list_samples_het.append(row[colname])
				elif hom in val:
					cases_tau_pathies_list_samples_hom.append(row[colname])

			### for cases_Secondary_parkinsonism_vascular_parkinsonism sample
			if colname in cases_Secondary_parkinsonism_vascular_parkinsonism_array_samples :
				cases_Secondary_parkinsonism_vascular_parkinsonism_list_samples.append(row[colname])
				if het in val:
					cases_Secondary_parkinsonism_vascular_parkinsonism_list_samples_het.append(row[colname])
				elif hom in val:
					cases_Secondary_parkinsonism_vascular_parkinsonism_list_samples_hom.append(row[colname])


			##########################
			######## CONTROLS ########
			##########################
			### for control sample
			if colname in control_array_samples :
				control_list_samples.append(row[colname])
				if het in val:
					control_list_samples_het.append(row[colname])
				elif hom in val:
					control_list_samples_hom.append(row[colname])

			### for control_inf_age_50 sample
			if colname in control_inf_age_50_array_samples :
				control_inf_age_50_list_samples.append(row[colname])
				if het in val:
					control_inf_age_50_list_samples_het.append(row[colname])
				elif hom in val:
					control_inf_age_50_list_samples_hom.append(row[colname])

			### for control_sup_age_50 sample
			if colname in control_sup_age_50_array_samples :
				control_sup_age_50_list_samples.append(row[colname])
				if het in val:
					control_sup_age_50_list_samples_het.append(row[colname])
				elif hom in val:
					control_sup_age_50_list_samples_hom.append(row[colname])

			#####################################
			######## unspecified_disease ########
			#####################################
			if colname in unspecified_disease_array_samples :
				unspecified_disease_list_samples.append(row[colname])
				if het in val:
					unspecified_disease_list_samples_het.append(row[colname])
				elif hom in val:
					unspecified_disease_list_samples_hom.append(row[colname])



			##########################
			######## NO PHENO ########
			##########################	
			### for no pheno sample
			if colname in nopheno_array_samples :
				nopheno_list_samples.append(row[colname])
				if het in val:
					nopheno_list_samples_het.append(row[colname])
				elif hom in val:
					nopheno_list_samples_hom.append(row[colname])


			i += 1

		### ALL
		### for all samples
		test = numpy.nan_to_num(array_samples)
		test=map(str,test)
		cleanedList = [x for x in test if x != 'nan']
		count_samples = len(cleanedList)
		if any("0.0" in s for s in cleanedList):
			count_samples = 0
		samples_liste_array.append(cleanedList)
		samples_count_array.append(count_samples)

		 ### for all samples het
		test_het = numpy.nan_to_num(array_samples_het)
		test_het=map(str,test_het)
		test_het_cleanedList = [x for x in test_het if x != 'nan']
		count_samples_het = len(test_het_cleanedList)
		if any("0.0" in s for s in test_het_cleanedList):
			count_samples_het = 0
		samples_array_samples_het.append(test_het_cleanedList)
		samples_count_array_het.append(count_samples_het)

		 ### for all samples hom
		test_hom = numpy.nan_to_num(array_samples_hom)
		test_hom=map(str,test_hom)
		test_hom_cleanedList = [x for x in test_hom if x != 'nan']
		count_samples_hom = len(test_hom_cleanedList)
		if any("0.0" in s for s in test_hom_cleanedList):
			count_samples_hom = 0
		samples_array_samples_hom.append(test_hom_cleanedList)
		samples_count_array_hom.append(count_samples_hom)


		#######################
		######## CASES ########
		#######################
		### for cases sample
		test2 = numpy.nan_to_num(cases_list_samples)
		test2=map(str,test2)
		cleanedList2 = [x for x in test2 if x != 'nan']
		count_samples2 = len(cleanedList2)
		if any("0.0" in s for s in cleanedList2):
			count_samples2 = 0
		cases_samples_liste_array.append(cleanedList2)
		cases_samples_count_array.append(count_samples2)
		 ### for cases sample het
		test2_het = numpy.nan_to_num(cases_list_samples_het)
		test2_het=map(str,test2_het)
		cleanedList2_het = [x for x in test2_het if x != 'nan']
		count_samples2_het = len(cleanedList2_het)
		if any("0.0" in s for s in cleanedList2_het):
			count_samples2_het = 0
		cases_samples_liste_array_het.append(cleanedList2_het)
		cases_samples_count_array_het.append(count_samples2_het)
		 ### for cases sample hom
		test2_hom = numpy.nan_to_num(cases_list_samples_hom)
		test2_hom=map(str,test2_hom)
		cleanedList2_hom = [x for x in test2_hom if x != 'nan']
		count_samples2_hom = len(cleanedList2_hom)
		if any("0.0" in s for s in cleanedList2_hom):
			count_samples2_hom = 0
		cases_samples_liste_array_hom.append(cleanedList2_hom)
		cases_samples_count_array_hom.append(count_samples2_hom)


		### for cases_alphasyn sample
		test2_cases_alphasyn = numpy.nan_to_num(cases_alphasyn_list_samples)
		test2_cases_alphasyn=map(str,test2_cases_alphasyn)
		cleanedList2_cases_alphasyn = [x for x in test2_cases_alphasyn if x != 'nan']
		count_samples2_cases_alphasyn = len(cleanedList2_cases_alphasyn)
		if any("0.0" in s for s in cleanedList2_cases_alphasyn):
			count_samples2_cases_alphasyn = 0
		cases_alphasyn_samples_liste_array.append(cleanedList2_cases_alphasyn)
		cases_alphasyn_samples_count_array.append(count_samples2_cases_alphasyn)
		 ### for cases_alphasyn sample het
		test2_cases_alphasyn_het = numpy.nan_to_num(cases_alphasyn_list_samples_het)
		test2_cases_alphasyn_het=map(str,test2_cases_alphasyn_het)
		cleanedList2_cases_alphasyn_het = [x for x in test2_cases_alphasyn_het if x != 'nan']
		count_samples2_cases_alphasyn_het = len(cleanedList2_cases_alphasyn_het)
		if any("0.0" in s for s in cleanedList2_cases_alphasyn_het):
			count_samples2_cases_alphasyn_het = 0
		cases_alphasyn_samples_liste_array_het.append(cleanedList2_cases_alphasyn_het)
		cases_alphasyn_samples_count_array_het.append(count_samples2_cases_alphasyn_het)
		 ### for cases_alphasyn sample hom
		test2_cases_alphasyn_hom = numpy.nan_to_num(cases_alphasyn_list_samples_hom)
		test2_cases_alphasyn_hom=map(str,test2_cases_alphasyn_hom)
		cleanedList2_cases_alphasyn_hom = [x for x in test2_cases_alphasyn_hom if x != 'nan']
		count_samples2_cases_alphasyn_hom = len(cleanedList2_cases_alphasyn_hom)
		if any("0.0" in s for s in cleanedList2_cases_alphasyn_hom):
			count_samples2_cases_alphasyn_hom = 0
		cases_alphasyn_samples_liste_array_hom.append(cleanedList2_cases_alphasyn_hom)
		cases_alphasyn_samples_count_array_hom.append(count_samples2_cases_alphasyn_hom)


		### for cases_tau_pathies sample
		test2_cases_tau_pathies = numpy.nan_to_num(cases_tau_pathies_list_samples)
		test2_cases_tau_pathies=map(str,test2_cases_tau_pathies)
		cleanedList2_cases_tau_pathies = [x for x in test2_cases_tau_pathies if x != 'nan']
		count_samples2_cases_tau_pathies = len(cleanedList2_cases_tau_pathies)
		if any("0.0" in s for s in cleanedList2_cases_tau_pathies):
			count_samples2_cases_tau_pathies = 0
		cases_tau_pathies_samples_liste_array.append(cleanedList2_cases_tau_pathies)
		cases_tau_pathies_samples_count_array.append(count_samples2_cases_tau_pathies)
		 ### for cases_tau_pathies sample het
		test2_cases_tau_pathies_het = numpy.nan_to_num(cases_tau_pathies_list_samples_het)
		test2_cases_tau_pathies_het=map(str,test2_cases_tau_pathies_het)
		cleanedList2_cases_tau_pathies_het = [x for x in test2_cases_tau_pathies_het if x != 'nan']
		count_samples2_cases_tau_pathies_het = len(cleanedList2_cases_tau_pathies_het)
		if any("0.0" in s for s in cleanedList2_cases_tau_pathies_het):
			count_samples2_cases_tau_pathies_het = 0
		cases_tau_pathies_samples_liste_array_het.append(cleanedList2_cases_tau_pathies_het)
		cases_tau_pathies_samples_count_array_het.append(count_samples2_cases_tau_pathies_het)
		 ### for cases_tau_pathies sample hom
		test2_cases_tau_pathies_hom = numpy.nan_to_num(cases_tau_pathies_list_samples_hom)
		test2_cases_tau_pathies_hom=map(str,test2_cases_tau_pathies_hom)
		cleanedList2_cases_tau_pathies_hom = [x for x in test2_cases_tau_pathies_hom if x != 'nan']
		count_samples2_cases_tau_pathies_hom = len(cleanedList2_cases_tau_pathies_hom)
		if any("0.0" in s for s in cleanedList2_cases_tau_pathies_hom):
			count_samples2_cases_tau_pathies_hom = 0
		cases_tau_pathies_samples_liste_array_hom.append(cleanedList2_cases_tau_pathies_hom)
		cases_tau_pathies_samples_count_array_hom.append(count_samples2_cases_tau_pathies_hom)

		### for cases_Secondary_parkinsonism_vascular_parkinsonism sample
		test2_cases_Secondary_parkinsonism_vascular_parkinsonism = numpy.nan_to_num(cases_Secondary_parkinsonism_vascular_parkinsonism_list_samples)
		test2_cases_Secondary_parkinsonism_vascular_parkinsonism=map(str,test2_cases_Secondary_parkinsonism_vascular_parkinsonism)
		cleanedList2_cases_Secondary_parkinsonism_vascular_parkinsonism = [x for x in test2_cases_Secondary_parkinsonism_vascular_parkinsonism if x != 'nan']
		count_samples2_cases_Secondary_parkinsonism_vascular_parkinsonism = len(cleanedList2_cases_Secondary_parkinsonism_vascular_parkinsonism)
		if any("0.0" in s for s in cleanedList2_cases_Secondary_parkinsonism_vascular_parkinsonism):
			count_samples2_cases_Secondary_parkinsonism_vascular_parkinsonism = 0
		cases_Secondary_parkinsonism_vascular_parkinsonism_samples_liste_array.append(cleanedList2_cases_Secondary_parkinsonism_vascular_parkinsonism)
		cases_Secondary_parkinsonism_vascular_parkinsonism_samples_count_array.append(count_samples2_cases_Secondary_parkinsonism_vascular_parkinsonism)
		 ### for cases_Secondary_parkinsonism_vascular_parkinsonism sample het
		test2_cases_Secondary_parkinsonism_vascular_parkinsonism_het = numpy.nan_to_num(cases_Secondary_parkinsonism_vascular_parkinsonism_list_samples_het)
		test2_cases_Secondary_parkinsonism_vascular_parkinsonism_het=map(str,test2_cases_Secondary_parkinsonism_vascular_parkinsonism_het)
		cleanedList2_cases_Secondary_parkinsonism_vascular_parkinsonism_het = [x for x in test2_cases_Secondary_parkinsonism_vascular_parkinsonism_het if x != 'nan']
		count_samples2_cases_Secondary_parkinsonism_vascular_parkinsonism_het = len(cleanedList2_cases_Secondary_parkinsonism_vascular_parkinsonism_het)
		if any("0.0" in s for s in cleanedList2_cases_Secondary_parkinsonism_vascular_parkinsonism_het):
			count_samples2_cases_Secondary_parkinsonism_vascular_parkinsonism_het = 0
		cases_Secondary_parkinsonism_vascular_parkinsonism_samples_liste_array_het.append(cleanedList2_cases_Secondary_parkinsonism_vascular_parkinsonism_het)
		cases_Secondary_parkinsonism_vascular_parkinsonism_samples_count_array_het.append(count_samples2_cases_Secondary_parkinsonism_vascular_parkinsonism_het)
		 ### for cases_Secondary_parkinsonism_vascular_parkinsonism sample hom
		test2_cases_Secondary_parkinsonism_vascular_parkinsonism_hom = numpy.nan_to_num(cases_Secondary_parkinsonism_vascular_parkinsonism_list_samples_hom)
		test2_cases_Secondary_parkinsonism_vascular_parkinsonism_hom=map(str,test2_cases_Secondary_parkinsonism_vascular_parkinsonism_hom)
		cleanedList2_cases_Secondary_parkinsonism_vascular_parkinsonism_hom = [x for x in test2_cases_Secondary_parkinsonism_vascular_parkinsonism_hom if x != 'nan']
		count_samples2_cases_Secondary_parkinsonism_vascular_parkinsonism_hom = len(cleanedList2_cases_Secondary_parkinsonism_vascular_parkinsonism_hom)
		if any("0.0" in s for s in cleanedList2_cases_Secondary_parkinsonism_vascular_parkinsonism_hom):
			count_samples2_cases_Secondary_parkinsonism_vascular_parkinsonism_hom = 0
		cases_Secondary_parkinsonism_vascular_parkinsonism_samples_liste_array_hom.append(cleanedList2_cases_Secondary_parkinsonism_vascular_parkinsonism_hom)
		cases_Secondary_parkinsonism_vascular_parkinsonism_samples_count_array_hom.append(count_samples2_cases_Secondary_parkinsonism_vascular_parkinsonism_hom)


		##########################
		######## CONTROLS ########
		##########################
		### for controls sample
		test3 = numpy.nan_to_num(control_list_samples)
		test3=map(str,test3)
		cleanedList3 = [x for x in test3 if x != 'nan']
		count_samples3 = len(cleanedList3)
		if any("0.0" in s for s in cleanedList3):
			count_samples3 = 0
		control_samples_liste_array.append(cleanedList3)
		control_samples_count_array.append(count_samples3)
		 ### for controls sample het
		test3_het = numpy.nan_to_num(control_list_samples_het)
		test3_het=map(str,test3_het)
		cleanedList3_het = [x for x in test3_het if x != 'nan']
		count_samples3_het = len(cleanedList3_het)
		if any("0.0" in s for s in cleanedList3_het):
			count_samples3_het = 0
		control_samples_liste_array_het.append(cleanedList3_het)
		control_samples_count_array_het.append(count_samples3_het)
		 ### for controls sample hom
		test3_hom = numpy.nan_to_num(control_list_samples_hom)
		test3_hom=map(str,test3_hom)
		cleanedList3_hom = [x for x in test3_hom if x != 'nan']
		count_samples3_hom = len(cleanedList3_hom)
		if any("0.0" in s for s in cleanedList3_hom):
			count_samples3_hom = 0
		control_samples_liste_array_hom.append(cleanedList3_hom)
		control_samples_count_array_hom.append(count_samples3_hom)		

		### for control_inf_age_50 sample
		test3_control_inf_age_50 = numpy.nan_to_num(control_inf_age_50_list_samples)
		test3_control_inf_age_50=map(str,test3_control_inf_age_50)
		cleanedList3_control_inf_age_50 = [x for x in test3_control_inf_age_50 if x != 'nan']
		count_samples3_control_inf_age_50 = len(cleanedList3_control_inf_age_50)
		if any("0.0" in s for s in cleanedList3_control_inf_age_50):
			count_samples3_control_inf_age_50 = 0
		control_inf_age_50_samples_liste_array.append(cleanedList3_control_inf_age_50)
		control_inf_age_50_samples_count_array.append(count_samples3_control_inf_age_50)
		 ### for control_inf_age_50s sample het
		test3_control_inf_age_50_het = numpy.nan_to_num(control_inf_age_50_list_samples_het)
		test3_control_inf_age_50_het=map(str,test3_control_inf_age_50_het)
		cleanedList3_control_inf_age_50_het = [x for x in test3_control_inf_age_50_het if x != 'nan']
		count_samples3_control_inf_age_50_het = len(cleanedList3_control_inf_age_50_het)
		if any("0.0" in s for s in cleanedList3_control_inf_age_50_het):
			count_samples3_control_inf_age_50_het = 0
		control_inf_age_50_samples_liste_array_het.append(cleanedList3_control_inf_age_50_het)
		control_inf_age_50_samples_count_array_het.append(count_samples3_control_inf_age_50_het)
		 ### for control_inf_age_50s sample hom
		test3_control_inf_age_50_hom = numpy.nan_to_num(control_inf_age_50_list_samples_hom)
		test3_control_inf_age_50_hom=map(str,test3_control_inf_age_50_hom)
		cleanedList3_control_inf_age_50_hom = [x for x in test3_control_inf_age_50_hom if x != 'nan']
		count_samples3_control_inf_age_50_hom = len(cleanedList3_control_inf_age_50_hom)
		if any("0.0" in s for s in cleanedList3_control_inf_age_50_hom):
			count_samples3_control_inf_age_50_hom = 0
		control_inf_age_50_samples_liste_array_hom.append(cleanedList3_control_inf_age_50_hom)
		control_inf_age_50_samples_count_array_hom.append(count_samples3_control_inf_age_50_hom)		

		### for control_sup_age_50 sample
		test3_control_sup_age_50 = numpy.nan_to_num(control_sup_age_50_list_samples)
		test3_control_sup_age_50=map(str,test3_control_sup_age_50)
		cleanedList3_control_sup_age_50 = [x for x in test3_control_sup_age_50 if x != 'nan']
		count_samples3_control_sup_age_50 = len(cleanedList3_control_sup_age_50)
		if any("0.0" in s for s in cleanedList3_control_sup_age_50):
			count_samples3_control_sup_age_50 = 0
		control_sup_age_50_samples_liste_array.append(cleanedList3_control_sup_age_50)
		control_sup_age_50_samples_count_array.append(count_samples3_control_sup_age_50)
		 ### for control_sup_age_50s sample het
		test3_control_sup_age_50_het = numpy.nan_to_num(control_sup_age_50_list_samples_het)
		test3_control_sup_age_50_het=map(str,test3_control_sup_age_50_het)
		cleanedList3_control_sup_age_50_het = [x for x in test3_control_sup_age_50_het if x != 'nan']
		count_samples3_control_sup_age_50_het = len(cleanedList3_control_sup_age_50_het)
		if any("0.0" in s for s in cleanedList3_control_sup_age_50_het):
			count_samples3_control_sup_age_50_het = 0
		control_sup_age_50_samples_liste_array_het.append(cleanedList3_control_sup_age_50_het)
		control_sup_age_50_samples_count_array_het.append(count_samples3_control_sup_age_50_het)
		 ### for control_sup_age_50s sample hom
		test3_control_sup_age_50_hom = numpy.nan_to_num(control_sup_age_50_list_samples_hom)
		test3_control_sup_age_50_hom=map(str,test3_control_sup_age_50_hom)
		cleanedList3_control_sup_age_50_hom = [x for x in test3_control_sup_age_50_hom if x != 'nan']
		count_samples3_control_sup_age_50_hom = len(cleanedList3_control_sup_age_50_hom)
		if any("0.0" in s for s in cleanedList3_control_sup_age_50_hom):
			count_samples3_control_sup_age_50_hom = 0
		control_sup_age_50_samples_liste_array_hom.append(cleanedList3_control_sup_age_50_hom)
		control_sup_age_50_samples_count_array_hom.append(count_samples3_control_sup_age_50_hom)		



		#####################################
		######## unspecified_disease ########
		#####################################
		unspecified_disease = numpy.nan_to_num(unspecified_disease_list_samples)
		unspecified_disease=map(str,unspecified_disease)
		unspecified_disease_cleanedList = [x for x in unspecified_disease if x != 'nan']
		unspecified_disease_count = len(unspecified_disease_cleanedList)
		if any("0.0" in s for s in unspecified_disease_cleanedList):
			unspecified_disease_count = 0
		unspecified_disease_samples_liste_array.append(unspecified_disease_cleanedList)
		unspecified_disease_samples_count_array.append(unspecified_disease_count)
		## for unspecified_disease sample het
		unspecified_disease_het = numpy.nan_to_num(unspecified_disease_list_samples_het)
		unspecified_disease_het=map(str,unspecified_disease_het)
		unspecified_disease_cleanedList_het = [x for x in unspecified_disease_het if x != 'nan']
		unspecified_disease_count_het = len(unspecified_disease_cleanedList_het)
		if any("0.0" in s for s in unspecified_disease_cleanedList_het):
			unspecified_disease_count_het = 0
		unspecified_disease_samples_liste_array_het.append(unspecified_disease_cleanedList_het)
		unspecified_disease_samples_count_array_het.append(unspecified_disease_count_het)
		## for nunspecified_disease sample hom
		unspecified_disease_hom = numpy.nan_to_num(unspecified_disease_list_samples_hom)
		unspecified_disease_hom=map(str,unspecified_disease_hom)
		unspecified_disease_cleanedList_hom = [x for x in unspecified_disease_hom if x != 'nan']
		unspecified_disease_count_hom = len(unspecified_disease_cleanedList_hom)
		if any("0.0" in s for s in unspecified_disease_cleanedList_hom):
			unspecified_disease_count_hom = 0
		unspecified_disease_samples_liste_array_hom.append(unspecified_disease_cleanedList_hom)
		unspecified_disease_samples_count_array_hom.append(unspecified_disease_count_hom)



		##########################
		######## NO PHENO ########
		##########################	
		## for no pheno sample
		test4 = numpy.nan_to_num(nopheno_list_samples)
		test4=map(str,test4)
		cleanedList4 = [x for x in test4 if x != 'nan']
		count_samples4 = len(cleanedList4)
		if any("0.0" in s for s in cleanedList4):
			count_samples4 = 0
		nopheno_samples_liste_array.append(cleanedList4)
		nopheno_samples_count_array.append(count_samples4)
		## for no pheno sample het
		test4_het = numpy.nan_to_num(nopheno_list_samples_het)
		test4_het=map(str,test4_het)
		cleanedList4_het = [x for x in test4_het if x != 'nan']
		count_samples4_het = len(cleanedList4_het)
		if any("0.0" in s for s in cleanedList4_het):
			count_samples4_het = 0
		nopheno_samples_liste_array_het.append(cleanedList4_het)
		nopheno_samples_count_array_het.append(count_samples4_het)
		## for no pheno sample hom
		test4_hom = numpy.nan_to_num(nopheno_list_samples_hom)
		test4_hom=map(str,test4_hom)
		cleanedList4_hom = [x for x in test4_hom if x != 'nan']
		count_samples4_hom = len(cleanedList4_hom)
		if any("0.0" in s for s in cleanedList4_hom):
			count_samples4_hom = 0
		nopheno_samples_liste_array_hom.append(cleanedList4_hom)
		nopheno_samples_count_array_hom.append(count_samples4_hom)



### ALL 
	### for all samples
	data['samples'] = samples_liste_array
	data['samples_count'] = samples_count_array
	data['samples'] = data['samples'].astype(str)
	data['samples'] = data['samples'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)

	### for all samples het
	data['samples_het'] = samples_array_samples_het
	data['samples_het_count'] = samples_count_array_het
	data['samples_het'] = data['samples_het'].astype(str)
	data['samples_het'] = data['samples_het'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)

	### for all samples hom
	data['samples_hom'] = samples_array_samples_hom
	data['samples_hom_count'] = samples_count_array_hom
	data['samples_hom'] = data['samples_hom'].astype(str)
	data['samples_hom'] = data['samples_hom'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)


	#######################
	######## CASES ########
	#######################
	### for cases samples
	data['cases_PD_PDD'] = cases_samples_liste_array
	data['cases_PD_PDD_count'] = cases_samples_count_array
	data['cases_PD_PDD'] = data['cases_PD_PDD'].astype(str)
	data['cases_PD_PDD'] = data['cases_PD_PDD'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for cases samples het
	data['cases_PD_PDD_het'] = cases_samples_liste_array_het
	data['cases_PD_PDD_het_count'] = cases_samples_count_array_het
	data['cases_PD_PDD_het'] = data['cases_PD_PDD_het'].astype(str)
	data['cases_PD_PDD_het'] = data['cases_PD_PDD_het'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for cases samples hom
	data['cases_PD_PDD_hom'] = cases_samples_liste_array_hom
	data['cases_PD_PDD_hom_count'] = cases_samples_count_array_hom
	data['cases_PD_PDD_hom'] = data['cases_PD_PDD_hom'].astype(str)
	data['cases_PD_PDD_hom'] = data['cases_PD_PDD_hom'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)

	### for cases_alphasyn samples
	data['cases_alphasyn'] = cases_alphasyn_samples_liste_array
	data['cases_alphasyn_count'] = cases_alphasyn_samples_count_array
	data['cases_alphasyn'] = data['cases_alphasyn'].astype(str)
	data['cases_alphasyn'] = data['cases_alphasyn'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for cases_alphasyn samples het
	data['cases_alphasyn_het'] = cases_alphasyn_samples_liste_array_het
	data['cases_alphasyn_het_count'] = cases_alphasyn_samples_count_array_het
	data['cases_alphasyn_het'] = data['cases_alphasyn_het'].astype(str)
	data['cases_alphasyn_het'] = data['cases_alphasyn_het'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for cases_alphasyn samples hom
	data['cases_alphasyn_hom'] = cases_alphasyn_samples_liste_array_hom
	data['cases_alphasyn_hom_count'] = cases_alphasyn_samples_count_array_hom
	data['cases_alphasyn_hom'] = data['cases_alphasyn_hom'].astype(str)
	data['cases_alphasyn_hom'] = data['cases_alphasyn_hom'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)

	### for cases_tau_pathies samples
	data['cases_tau_pathies'] = cases_tau_pathies_samples_liste_array
	data['cases_tau_pathies_count'] = cases_tau_pathies_samples_count_array
	data['cases_tau_pathies'] = data['cases_tau_pathies'].astype(str)
	data['cases_tau_pathies'] = data['cases_tau_pathies'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for cases_tau_pathies samples het
	data['cases_tau_pathies_het'] = cases_tau_pathies_samples_liste_array_het
	data['cases_tau_pathies_het_count'] = cases_tau_pathies_samples_count_array_het
	data['cases_tau_pathies_het'] = data['cases_tau_pathies_het'].astype(str)
	data['cases_tau_pathies_het'] = data['cases_tau_pathies_het'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for cases_tau_pathies samples hom
	data['cases_tau_pathies_hom'] = cases_tau_pathies_samples_liste_array_hom
	data['cases_tau_pathies_hom_count'] = cases_tau_pathies_samples_count_array_hom
	data['cases_tau_pathies_hom'] = data['cases_tau_pathies_hom'].astype(str)
	data['cases_tau_pathies_hom'] = data['cases_tau_pathies_hom'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)

	### for cases_Secondary_parkinsonism_vascular_parkinsonism samples
	data['cases_Secondary_parkinsonism_vascular_parkinsonism'] = cases_Secondary_parkinsonism_vascular_parkinsonism_samples_liste_array
	data['cases_Secondary_parkinsonism_vascular_parkinsonism_count'] = cases_Secondary_parkinsonism_vascular_parkinsonism_samples_count_array
	data['cases_Secondary_parkinsonism_vascular_parkinsonism'] = data['cases_Secondary_parkinsonism_vascular_parkinsonism'].astype(str)
	data['cases_Secondary_parkinsonism_vascular_parkinsonism'] = data['cases_Secondary_parkinsonism_vascular_parkinsonism'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for cases_Secondary_parkinsonism_vascular_parkinsonism samples het
	data['cases_Secondary_parkinsonism_vascular_parkinsonism_het'] = cases_Secondary_parkinsonism_vascular_parkinsonism_samples_liste_array_het
	data['cases_Secondary_parkinsonism_vascular_parkinsonism_het_count'] = cases_Secondary_parkinsonism_vascular_parkinsonism_samples_count_array_het
	data['cases_Secondary_parkinsonism_vascular_parkinsonism_het'] = data['cases_Secondary_parkinsonism_vascular_parkinsonism_het'].astype(str)
	data['cases_Secondary_parkinsonism_vascular_parkinsonism_het'] = data['cases_Secondary_parkinsonism_vascular_parkinsonism_het'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for cases_Secondary_parkinsonism_vascular_parkinsonism samples hom
	data['cases_Secondary_parkinsonism_vascular_parkinsonism_hom'] = cases_Secondary_parkinsonism_vascular_parkinsonism_samples_liste_array_hom
	data['cases_Secondary_parkinsonism_vascular_parkinsonism_hom_count'] = cases_Secondary_parkinsonism_vascular_parkinsonism_samples_count_array_hom
	data['cases_Secondary_parkinsonism_vascular_parkinsonism_hom'] = data['cases_Secondary_parkinsonism_vascular_parkinsonism_hom'].astype(str)
	data['cases_Secondary_parkinsonism_vascular_parkinsonism_hom'] = data['cases_Secondary_parkinsonism_vascular_parkinsonism_hom'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)


	##########################
	######## CONTROLS ########
	##########################
	### for controls samples
	data['controls'] = control_samples_liste_array
	data['controls_count'] = control_samples_count_array
	data['controls'] = data['controls'].astype(str)
	data['controls'] = data['controls'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for controls samples het
	data['controls_het'] = control_samples_liste_array_het
	data['controls_het_count'] = control_samples_count_array_het
	data['controls_het'] = data['controls_het'].astype(str)
	data['controls_het'] = data['controls_het'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for controls samples hom
	data['controls_hom'] = control_samples_liste_array_hom
	data['controls_hom_count'] = control_samples_count_array_hom
	data['controls_hom'] = data['controls_hom'].astype(str)
	data['controls_hom'] = data['controls_hom'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)


	### for control_inf_age_50 samples
	data['control_inf_age_50'] = control_inf_age_50_samples_liste_array
	data['control_inf_age_50_count'] = control_inf_age_50_samples_count_array
	data['control_inf_age_50'] = data['control_inf_age_50'].astype(str)
	data['control_inf_age_50'] = data['control_inf_age_50'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for control_inf_age_50 samples het
	data['control_inf_age_50_het'] = control_inf_age_50_samples_liste_array_het
	data['control_inf_age_50_het_count'] = control_inf_age_50_samples_count_array_het
	data['control_inf_age_50_het'] = data['control_inf_age_50_het'].astype(str)
	data['control_inf_age_50_het'] = data['control_inf_age_50_het'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for control_inf_age_50 samples hom
	data['control_inf_age_50_hom'] = control_inf_age_50_samples_liste_array_hom
	data['control_inf_age_50_hom_count'] = control_inf_age_50_samples_count_array_hom
	data['control_inf_age_50_hom'] = data['control_inf_age_50_hom'].astype(str)
	data['control_inf_age_50_hom'] = data['control_inf_age_50_hom'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)

	### for control_sup_age_50 samples
	data['control_sup_age_50'] = control_sup_age_50_samples_liste_array
	data['control_sup_age_50_count'] = control_sup_age_50_samples_count_array
	data['control_sup_age_50'] = data['control_sup_age_50'].astype(str)
	data['control_sup_age_50'] = data['control_sup_age_50'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for control_sup_age_50 samples het
	data['control_sup_age_50_het'] = control_sup_age_50_samples_liste_array_het
	data['control_sup_age_50_het_count'] = control_sup_age_50_samples_count_array_het
	data['control_sup_age_50_het'] = data['control_sup_age_50_het'].astype(str)
	data['control_sup_age_50_het'] = data['control_sup_age_50_het'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for control_sup_age_50 samples hom
	data['control_sup_age_50_hom'] = control_sup_age_50_samples_liste_array_hom
	data['control_sup_age_50_hom_count'] = control_sup_age_50_samples_count_array_hom
	data['control_sup_age_50_hom'] = data['control_sup_age_50_hom'].astype(str)
	data['control_sup_age_50_hom'] = data['control_sup_age_50_hom'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)


	#####################################
	######## unspecified_disease ########
	#####################################
	### for unspecified_disease sample
	data['unspecified_disease'] = unspecified_disease_samples_liste_array
	data['unspecified_disease_count'] = unspecified_disease_samples_count_array
	data['unspecified_disease'] = data['unspecified_disease'].astype(str)
	data['unspecified_disease'] = data['unspecified_disease'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for unspecified_disease sample het
	data['unspecified_disease_het'] = unspecified_disease_samples_liste_array_het
	data['unspecified_disease_het_count'] = unspecified_disease_samples_count_array_het
	data['unspecified_disease_het'] = data['unspecified_disease_het'].astype(str)
	data['nunspecified_disease_het'] = data['unspecified_disease_het'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for unspecified_disease sample hom
	data['unspecified_disease_hom'] = unspecified_disease_samples_liste_array_hom
	data['unspecified_disease_hom_count'] = unspecified_disease_samples_count_array_hom
	data['unspecified_disease_hom'] = data['unspecified_disease_hom'].astype(str)
	data['unspecified_disease_hom'] = data['unspecified_disease_hom'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)


	##########################
	######## NO PHENO ########
	##########################	
	### for no pheno sample
	data['new_list'] = nopheno_samples_liste_array
	data['new_list_count'] = nopheno_samples_count_array
	data['new_list'] = data['new_list'].astype(str)
	data['new_list'] = data['new_list'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for no pheno sample het
	data['new_list_het'] = nopheno_samples_liste_array_het
	data['new_list_het_count'] = nopheno_samples_count_array_het
	data['new_list_het'] = data['new_list_het'].astype(str)
	data['new_list_het'] = data['new_list_het'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)
	### for no pheno sample hom
	data['new_list_hom'] = nopheno_samples_liste_array_hom
	data['new_list_hom_count'] = nopheno_samples_count_array_hom
	data['new_list_hom'] = data['new_list_hom'].astype(str)
	data['new_list_hom'] = data['new_list_hom'].replace(to_replace=['^\[\'0.0.*$'],value=['0'], regex=True)



	mask = (~data['samples'].str.startswith('0'))
	data=data[mask]

	data.to_csv(output, index=False, sep='\t')

	print ("end function SamplesAaffected_and_SamplesCount_by_variants")


#####

annotate_genotype_info2(input_tsv, output, FORMAT)

SamplesAaffected_and_SamplesCount_by_variants(output + ".tmp", output, FORMAT, cases, controls, unspecified_disease, control_inf_age_50, control_sup_age_50, cases_alphasyn, cases_tau_pathies, cases_Secondary_parkinsonism_vascular_parkinsonism, no_pheno)


### remove temperory file
os.remove(output + ".tmp")


print ("END FULL python script")


