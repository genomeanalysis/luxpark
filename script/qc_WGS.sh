#!/usr/bin/bash

####################################################
#########  AUTHOR : Dheeraj Reddy BOBBILI  #########
####################################################


input_vcf=$1
ref=$2
vcf_name=$(basename $input_vcf | sed -E 's/(.vcf|.vcf.gz|.gvcf|.gvcf.gz)//g')
output_name=$vcf_name.tmp


# ref=./GRCh37.fa
# ref=./ucsc.hg19.fasta


##DECOMPOSE
decompose()
{
	echo "### Started decomposing"
	vt decompose $input_vcf -o $output_name.decomposed.vcf.gz
	tabix -fp vcf $output_name.decomposed.vcf.gz
	echo "### Done decompsing"	

}

##Change genotype for the decomposed variants otherwise the bcftools in the later steps are not working properly. It works only with a python version < 3
change_genotype()
{
	echo "### Started change_genotype"
	python2 ./change_genotype.py $output_name.decomposed.vcf.gz | bgzip -c > $output_name.decomposed.change_genotype.vcf.gz
	tabix -fp vcf $output_name.decomposed.change_genotype.vcf.gz
	echo "### Done change_genotype"
}


###NORMALIZE
normalize()
{
	echo "### Started normalizing"
	bcftools norm -f $ref  -c w $output_name.decomposed.change_genotype.vcf.gz -o $output_name.decomposed.change_genotype.normalised.vcf.gz -O z
	tabix -fp vcf $output_name.decomposed.change_genotype.normalised.vcf.gz
	echo "### Started normalizing"
}


exclude_uncalled()
{
	echo "###  Started exclude uncalled"
	bcftools view --exclude-uncalled -Oz -o $output_name.decomposed.change_genotype.normalised.called.vcf.gz $output_name.decomposed.change_genotype.normalised.vcf.gz
	tabix -fp vcf $output_name.decomposed.change_genotype.normalised.called.vcf.gz
	echo "###  Done exclude uncalled"
}


##########################
# ANNOTATE VARIANTS
##########################
annotate_with_filters()
{
	gatk VariantAnnotator \
        --java-options '-DGATK_STACKTRACE_ON_USER_EXCEPTION=true' \
		--reference $REFGENOME \
		--variant $output_name.decomposed.change_genotype.normalised.called.vcf.gz \
        --annotation-group AlleleSpecificAnnotation \
        --annotation-group AS_StandardAnnotation \
        --annotation-group ReducibleAnnotation \
        --annotation-group StandardAnnotation \
        --annotation-group StandardHCAnnotation \
		--output $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.vcf.gz \
		--intervals $script_dir/Exome_interval.hg19.100bp.list
}


##########################
# FILTER SNPs
##########################


select_snps()
{
	gatk SelectVariants \
		-R $REFGENOME \
		--variant $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.vcf.gz \
		--output $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.SNP.vcf.gz \
		--select-type-to-include SNP \
		--exclude-non-variants
}


## qc on ref alleles
qc_snps()
{
	echo "### Started SNP_QC"	

	### remove GQ<20  
	bcftools view  -i  'MIN(FORMAT/GQ)>20' -Oz -o $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.SNP.GQ_filter.vcf.gz  $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.SNP.vcf.gz



	gatk VariantFiltration \
	-R $ref \
	-V $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.SNP.GQ_filter.vcf.gz  \
	-O $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.SNP.GQ_filter.hard_filter.vcf.gz \
		-filter "QD < 2.0" \
		--filter-name "QD_filter" \
		-filter "FS > 60.0" \
		--filter-name "FS_filter" \
		-filter "MQ < 40.0" \
		--filter-name "MQ_filter" \
		-filter "MQRankSum < -12.5" \
		--filter-name "MQRankSum_filter" \
		-filter "ReadPosRankSum < -8.0" \
		--filter-name "ReadPosRankSum_filter" \
		-filter "DP < 10.0" \
		--filter-name "DP_filter" \
		-filter "(ABHet > 0.75 && ABHet < 0.99) || ABHet < 0.25" \
		--filter-name "AB_filter" \
		-filter "QUAL < 30.0" \
		--filter-name "QUAL30_filter" \
		-filter "SOR > 3.0" \
		--filter-name "SOR_filter" \
        --filter-name "LOD_filter" \
        --filter-expression "LOD < 0.0" \
		--verbosity ERROR

	# Remove filtered positions
	vcftools --gzvcf $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.SNP.GQ_filter.hard_filter.vcf.gz --recode --recode-INFO-all --remove-filtered-all --minGQ 20 --stdout | bgzip -c > $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.SNP.QC.vcf.gz

	tabix -fp vcf $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.SNP.QC.vcf.gz

	echo "### Done SNP_QC"	

}


select_indels()
{
	echo "### Started select_indels"
	gatk SelectVariants \
	-R $ref \
	-V $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.vcf.gz \
	--select-type-to-include INDEL \
	-O $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.INDEL.vcf.gz
	echo "### Done select_indels"
}

qc_indels()
{
	echo "### Started qc_indels"
	gatk VariantFiltration \
	-R $ref \
	-V $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.INDEL.vcf.gz \
	-O $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.INDEL.hard_filter.vcf.gz \
		-filter "QD < 2.0" \
		--filter-name "QD_filter" \
		-filter "FS > 200.0" \
		--filter-name "FS_filter" \
		-filter "ReadPosRankSum < -20.0" \
		--filter-name "ReadPosRankSum_filter" \
		-filter "DP < 10.0" \
		--filter-name "DP_filter" \
		-filter "QUAL < 30.0" \
		--filter-name "QUAL30" \
		--verbosity ERROR

	vcftools --gzvcf $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.INDEL.hard_filter.vcf.gz --recode --recode-INFO-all --remove-filtered-all --stdout | bgzip -c > $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.INDEL.QC.vcf.gz

	tabix -fp vcf $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.INDEL.QC.vcf.gz
	echo "### Done qc_indels"
}

combine_snp_indels()
{
	echo "### Started combine_snp_indels"

	bcftools concat -a --output-type z --output $output_name.QC.combine_snp_indels.vcf.gz $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.SNP.QC.vcf.gz $output_name.decomposed.change_genotype.normalised.called.Qual.annotated.INDEL.QC.vcf.gz

	echo "### Done combine_snp_indels"
}


# Exclude all the non variant sites
filter_nonvariants()
{
	gatk SelectVariants \
		-R $REFGENOME \
		--variant $output_name.QC.combine_snp_indels.vcf.gz \
		--output $vcf_name.QCed.vcf \
		--exclude-non-variants
}



######### 
decompose
change_genotype
normalize
exclude_uncalled
annotate_with_filters
select_snps
qc_snps
select_indels
qc_indels
combine_snp_indels
filter_nonvariants

echo "END QC"

