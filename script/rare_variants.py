#!/home/users/spachchek/miniconda3/bin/python


######################################################
#########  AUTHOR : Sinthuja PACHCHEK PEIRIS #########
######################################################

# python ./rare_variants.py input_tsv [0.05|0.01] # output "rare_variant_MAF_inf_0.05.$input"


import sys
import pandas
import csv
import numpy

input_tsv= sys.argv[1]
MAF_input = sys.argv[2]

MAF = float(MAF_input)

output="rare_variant_MAF_inf_"+ MAF_input + "."

print (output)

def rare_variants(input_tsv, MAF):


	data = pandas.read_csv(input_tsv, delimiter='\t', dtype={'CHR': 'str'}, na_values=['.', 'NA', 'nan'])

	mask_pop_freq = (data['gnomAD_genome_NFE'] <= MAF) & (data['gnomAD_exome_NFE'] <= MAF) | (data['gnomAD_genome_NFE'].isnull()) & (data['gnomAD_exome_NFE'].isnull()) | (data['gnomAD_genome_NFE'] <= MAF) & (data['gnomAD_exome_NFE'].isnull()) | (data['gnomAD_genome_NFE'].isnull()) & (data['gnomAD_exome_NFE'] <= MAF)


	data_mask_pop_freq_rare_variant_gnomAD_NFE=data[mask_pop_freq]

	data_mask_pop_freq_rare_variant_gnomAD_NFE.to_csv(output + input_tsv, index=False, sep='\t')



rare_variants(input_tsv, MAF)


### python /Users/sinthuja.pachchek/Documents/PhD/scripts/filter_tsv/select_rare_variants.py /Users/sinthuja.pachchek/Downloads/test.tsv /Users/sinthuja.pachchek/Downloads/output.tsv 
