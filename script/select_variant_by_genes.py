#!/home/users/spachchek/miniconda3/bin/python

######################################################
#########  AUTHOR : Sinthuja PACHCHEK PEIRIS #########
######################################################

# python ./select_variant_by_genes.py input_tsv /home/users/spachchek/DATA/PD_genes.ncbi.UCL.txt output_tsv


import sys
import pandas
import csv
import numpy
import os
from sh import sed


input_tsv = sys.argv[1]
list_genes = sys.argv[2]
output = sys.argv[3]


def select_variant_by_genes(input_tsv, output, list_genes):
        data = pandas.read_csv(input_tsv, delimiter='\t', dtype={'CHR': 'str'}, na_values=['.', 'NA', 'nan'])

        array_gene = []
        with open(list_genes, 'r') as genes_file:
                for line in genes_file:
                        gene=line.strip()
                        array_gene.append(gene)

        new = data['Gene.refGene'].str.split(';', n = 1, expand = True)
        data["Gene1"]= new[0]
        data["Gene2"]= new[1]

        mask = (data["Gene1"].isin(array_gene)) | (data["Gene2"].isin(array_gene))

        data=data[mask]

        data2=data.drop(columns=['Gene1', 'Gene2'])

        data2.to_csv(output, index=False, sep='\t')

select_variant_by_genes(input_tsv, output, list_genes)