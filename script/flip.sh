#!/usr/bin/bash


#################################################################################
#########  AUTHOR : Dheeraj Reddy BOBBILI  and Sinthuja PACHCHEK PEIRIS #########
#################################################################################


# ./flip.sh input_vcf [hg19|GRCh37]

input_vcf=$1
output_dir=$(dirname $input_vcf)
vcf_name=$(basename $input_vcf |sed -E 's/(.vcf|.vcf.gz)//g')
output_name=$output_dir/$vcf_name.tmp

ref=$2

# ref=./GRCh37/seq/GRCh37.fa
# ref=./resources_for_GATK/ucsc.hg19.fasta

if [[ $ref == "hg19" ]]
then
    REFGENOM=./ucsc.hg19.fasta
elif [[ $ref == "GRCh37" ]]
    then
    REFGENOM=./seq/GRCh37.fa
fi


# ### Collapse duplicate variants to single line
python ./collapse_variants.py $input_vcf  # output : input.clean.vcf and multiple_ids_per_variant.tsv
mv $vcf_name.clean.vcf $output_name.clean.vcf


if [[ $ref == "hg19" ]]
then
    ## remove error chromosomes
    vcftools --vcf $output_name.clean.vcf --not-chr chr0 --not-chr chrXY --not-chr chrM --not-chr chrMT --recode --recode-INFO-all --out $output_name.filtred.clean.vcf
elif [[ $ref == "GRCh37" ]]
    then
    ## remove error chromosomes
    vcftools --vcf $output_name.clean.vcf --not-chr 0 --not-chr XY --not-chr M --not-chr MT --recode --recode-INFO-all --out $output_name.filtred.clean.vcf
fi


mkdir -p tmp
cat $output_name.filtred.clean.vcf.recode.vcf | vcf-sort -t tmp | bgzip -c > $output_name.2.clean.vcf.gz
tabix -fp vcf $output_name.2.clean.vcf.gz

### Convert the ref alt alleles according to reference genome, but this also is generating random positions which needs to be swapped from top to bottom strand
bcftools norm --check-ref ws -f $REFGENOM -o $output_name.3.vcf -Ov $output_name.2.clean.vcf.gz 

## Get the list of top bottom problematic positions from file above
python ./print_prob_snps.py $output_name.3.vcf

## Flip the top and bottom strands
plink --vcf $output_name.2.clean.vcf.gz --double-id --make-bed --out $output_name.4

if [[ $ref == "hg19" ]]
then
    plink --bfile $output_name.4 --flip $output_name.3.vcf.fliplist.txt --output-chr chrM --recode vcf-iid --out $output_name.5
elif [[ $ref == "GRCh37" ]]
    then
    plink --bfile $output_name.4 --flip $output_name.3.vcf.fliplist.txt --output-chr M --recode vcf-iid --out $output_name.5
fi


cat $output_name.5.vcf | vcf-sort -t tmp | bgzip -c > $output_name.5.vcf.gz
tabix -fp vcf $output_name.5.vcf.gz

# ### After fixing the top and bottom strand now the ref alt alleles are changed acocording to the reference fasta
 bcftools norm --check-ref ws -f $REFGENOM -o $output_name.6.vcf -Ov $output_name.5.vcf.gz 


mv $output_name.6.vcf $vcf_name.flip.vcf
rm multiple_ids_per_variant.tsv
rm $output_name*
rm -r tmp/


echo "END OF FLIP  SCRIPT"