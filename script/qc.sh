#!/bin/bash -l
######################################################
#########  AUTHOR : Sinthuja PACHCHEK PEIRIS #########
######################################################

# ./qc.sh vcf_input

input_vcf=$1
output_dir=$(dirname $input_vcf)
vcf_name=$(basename $input_vcf | sed 's/.vcf//')
output_name=$output_dir/$vcf_name.tmp


qc.step ()
{
	### convert vcf to plink
	plink --vcf $input_vcf --double-id --make-bed --out $output_name
	plink --bfile $output_name --make-bed --out $output_name.1
	plink --bfile $output_name.1 --recode vcf-iid --output-chr chrM --out $vcf_name.QCed.vcf --geno 0.05 --mind 0.05 --chr 1-22,X,Y --hwe 1e-6

}

######
qc.step

echo "END OF QC"
