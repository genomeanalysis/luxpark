#!/usr/bin/bash

######################################################
#########  AUTHOR : Sinthuja PACHCHEK PEIRIS #########
######################################################


# ./Annovar.Annotation.sh input_vcf [hg19|hg38] [full|basic]


vcffile=$1
buildver=$2
mode=$3


threads=10
vcf_name=$(basename $vcffile)
output=$(basename $vcffile | sed -E 's/(.vcf|.vcf.gz)//g')


ANNOVAR=./annovar
ANNOVAR_DB=./humandb

#### hg19
### full
full_protocol_hg19="refGene,gnomad_genome,gnomad_exome,avsnp150,snp138NonFlagged,clinvar_20210123,hgmd32020,cadd13,caddindel,caddgt20,dann,dbscsnv11,cosmic70,ccdsGene,knownGene,wgEncodeGencodeManualV4,NARD,gme,kaviar_20150923,hrcr1,esp6500si_all,esp6500si_ea,esp6500siv2_all,esp6500siv2_ea,cg46,cg69,1000g2015aug_all,1000g2015aug_eur,exac03,exac03nonpsych,exac03nontcga,DiscovEHR,popfreq_max,phastConsElements46way,segdup,cytoband,dgv,dgvMerged,tfbs,gwascatalog,wgEncodeRegTfbsClustered,wgEncodeRegDnaseClustered,wgEncodeBroadHmmGm12878HMM,wgEncodeAwgSegmentationChromhmmGm12878,mirna,mirnatarget,cosmic68wgs,nci60,dbnsfp41a,regsnpintron,condel,gerp++gt2,spidex,GTeXbrain,Maurano2015,fathmm,gwava,eigen,PTMsites,revel,icgc28,mcap13,UP000005640_9606_act_site,UP000005640_9606_binding,UP000005640_9606_ca_bind,UP000005640_9606_carbohyd,UP000005640_9606_coiled,UP000005640_9606_crosslnk,UP000005640_9606_disulfid,UP000005640_9606_dna_bind,UP000005640_9606_domain,UP000005640_9606_intramem,UP000005640_9606_lipid,UP000005640_9606_metal,UP000005640_9606_mod_res,UP000005640_9606_motif,UP000005640_9606_non_std,UP000005640_9606_np_bind,UP000005640_9606_peptide,UP000005640_9606_propep,UP000005640_9606_region,UP000005640_9606_repeat,UP000005640_9606_signal,UP000005640_9606_site,UP000005640_9606_topo_dom,UP000005640_9606_transit,UP000005640_9606_transmem,UP000005640_9606_variant,UP000005640_9606_zn_fing,oreganno,tRNA,lincRNAs,polyadb,cpgIslandExt,microsat,MIRNAs,transfac,subgerpexonpercentile,subrvisexonpercentile,subgerpdomainpercentile,subrvisdomainpercentile,dbnsfp31a_interpro,rmsk,parazscore,abraom,intervar_20180118,PER,cadd13gt10"
full_operation_hg19="g,f,f,f,f,f,f,f,f,f,f,f,f,g,g,g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,r,r,r,r,r,r,r,r,r,r,r,r,r,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,f,r,f,f,f,f,f"
# full_arg_hg19="'--splicing_threshold 20 --exonicsplicing --neargene 4000','--splicing_threshold 20 --exonicsplicing --neargene 4000',,,,,,,,,,,,,'--splicing_threshold 20 --exonicsplicing --neargene 4000','--splicing_threshold 20 --exonicsplicing --neargene 4000','--splicing_threshold 20 --exonicsplicing --neargene 4000',,,,,,,,,,,,,,,,,,,,,'-colsWanted 11','-colsWanted 10',,,'-scorecolumn 11','-scorecolumn 5',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,"


## basic
basic_protocol_hg19="refGene,gnomad_genome,gnomad_exome,avsnp150,snp138NonFlagged,clinvar_20210123,hgmd32020,cadd13,caddindel,caddgt20,dann,dbscsnv11,cosmic70,dbnsfp41a"
basic_operation_hg19="g,f,f,f,f,f,f,f,f,f,f,f,f,f"
# basic_arg_hg19="'--splicing_threshold 20 --exonicsplicing --neargene 4000','--splicing_threshold 20 --exonicsplicing --neargene 4000',,,,,,,,,,,,"


#### hg38
### full
full_protocol_hg38="refGene,gnomad30_genome,avsnp150,clinvar_20210123,hgmd32020,dbscsnv11,cosmic70,knownGene,gme,hrcr1,cytoBand,nci60,dbnsfp41a,regsnpintron,revel,icgc28,MIRNAs,transfac,abraom,intervar_20180118"
full_operation_hg38="g,f,f,f,f,f,f,g,f,f,r,f,f,f,f,f,r,r,f,f"
# full_arg_hg38="'--splicing_threshold 20 --exonicsplicing --neargene 4000','--splicing_threshold 20 --exonicsplicing --neargene 4000',,,,,,,'--splicing_threshold 20 --exonicsplicing --neargene 4000',,,,,,,,,,,,,,,,"

## basic
basic_protocol_hg38="refGene,gnomad30_genome,avsnp150,clinvar_20210123,hgmd32020,dbscsnv11,cosmic70,dbnsfp41a"
basic_operation_hg38="g,f,f,f,f,f,f,f"
basic_arg_hg38="'--splicing_threshold 20 --exonicsplicing --neargene 4000','--splicing_threshold 20 --exonicsplicing --neargene 4000',,,,,,,"





protocol="error"
operation="error"
# arg="error"

if [[ $buildver == "hg19" ]]; then
	if [[ $mode == "full" ]]; then
		protocol=$full_protocol_hg19
		operation=$full_operation_hg19
		# arg=$full_arg_hg19
		if [[ protocol != "error" ]] & [[ operation != "error" ]] ; then

			if [[ $vcffile == *"vcf"* ]]; then
				perl $ANNOVAR/table_annovar.pl $vcffile $ANNOVAR_DB --thread $threads --buildver $buildver --vcfinput -protocol $protocol -operation $operation --argument '--splicing_threshold 20 --neargene 4000',,,,,,,,,,,,,'--splicing_threshold 20 --neargene 4000','--splicing_threshold 20 --neargene 4000','--splicing_threshold 20 --neargene 4000',,,,,,,,,,,,,,,,,,,,,'-colsWanted 11','-colsWanted 10',,,'-scorecolumn 11','-scorecolumn 5',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,, -remove -polish -outfile $output.annotated -nastring .

			else
				perl $ANNOVAR/table_annovar.pl $vcffile $ANNOVAR_DB --thread $threads --buildver $buildver -protocol $protocol -operation $operation --argument '--splicing_threshold 20 --neargene 4000',,,,,,,,,,,,,'--splicing_threshold 20 --neargene 4000','--splicing_threshold 20 --neargene 4000','--splicing_threshold 20 --neargene 4000',,,,,,,,,,,,,,,,,,,,,'-colsWanted 11','-colsWanted 10',,,'-scorecolumn 11','-scorecolumn 5',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,, -remove -polish -outfile $output.annotated -nastring . --otherinfo
			fi
		else

			echo "ERROR on arguments"
		fi

	elif [[ $mode == "basic" ]]; then
		protocol=$basic_protocol_hg19
		operation=$basic_operation_hg19
		# arg="$basic_arg_hg19"
		if [[ protocol != "error" ]] & [[ operation != "error" ]] ; then

			if [[ $vcffile == *"vcf"* ]]; then
				perl $ANNOVAR/table_annovar.pl $vcffile $ANNOVAR_DB --thread $threads --buildver $buildver --vcfinput -protocol $protocol -operation $operation --argument '--splicing_threshold 20 --neargene 4000',,,,,,,,,,,,, -remove -polish -outfile $output.annotated -nastring .

			else
				perl $ANNOVAR/table_annovar.pl $vcffile $ANNOVAR_DB --thread $threads --buildver $buildver -protocol $protocol -operation $operation --argument '--splicing_threshold 20 --neargene 4000',,,,,,,,,,,,, -remove -polish -outfile $output.annotated -nastring . --otherinfo
			fi
		else

			echo "ERROR on arguments"
		fi


	fi
elif [[ $buildver == "hg38" ]]; then
	ANNOVAR_DB=./humandb/hg38

	if [[ $mode == "full" ]]; then
		protocol=$full_protocol_hg38
		operation=$full_operation_hg38
		# arg=$full_arg_hg38
		if [[ protocol != "error" ]] & [[ operation != "error" ]] ; then

			if [[ $vcffile == *"vcf"* ]]; then
				perl $ANNOVAR/table_annovar.pl $vcffile $ANNOVAR_DB --thread $threads --buildver $buildver --vcfinput -protocol $protocol -operation $operation --argument '--splicing_threshold 20 --neargene 4000',,,,,,,'--splicing_threshold 20 --neargene 4000',,,,,,,,,,,, -remove -polish -outfile $output.annotated -nastring .

			else
				perl $ANNOVAR/table_annovar.pl $vcffile $ANNOVAR_DB --thread $threads --buildver $buildver -protocol $protocol -operation $operation --argument '--splicing_threshold 20 --neargene 4000',,,,,,,'--splicing_threshold 20 --neargene 4000',,,,,,,,,,,, -remove -polish -outfile $output.annotated -nastring . --otherinfo
			fi
		else

			echo "ERROR on arguments"
		fi

	elif [[ $mode == "basic" ]]; then
		protocol=$basic_protocol_hg38
		operation=$basic_operation_hg38
		# arg=$basic_arg_hg38
		if [[ protocol != "error" ]] & [[ operation != "error" ]] ; then

			if [[ $vcffile == *"vcf"* ]]; then
				perl $ANNOVAR/table_annovar.pl $vcffile $ANNOVAR_DB --thread $threads --buildver $buildver --vcfinput -protocol $protocol -operation $operation --argument '--splicing_threshold 20 --neargene 4000',,,,,,, -remove -polish -outfile $output.annotated -nastring .

			else
				perl $ANNOVAR/table_annovar.pl $vcffile $ANNOVAR_DB --thread $threads --buildver $buildver -protocol $protocol -operation $operation --argument '--splicing_threshold 20 --neargene 4000',,,,,,, -remove -polish -outfile $output.annotated -nastring . --otherinfo
			fi
		else

			echo "ERROR on arguments"
		fi
	fi
fi

echo "END ANNOVAR ANNOTATION"

