#!/usr/bin/bash

######################################################
#########  AUTHOR : Sinthuja PACHCHEK PEIRIS #########
######################################################


# ./convertVCF2TSV.sh input_vcf [hg19|hg38] [full|basic|CNV]



input_vcf=$1
buildver=$2
mode=$3


input_name=$(basename $input_vcf)
output=$(basename $input_vcf | sed -E 's/(.vcf|.vcf.gz)//g')
script_path=./convert_to_tsv

cols="error"

if [[ $buildver == "hg19" ]]; then
	if [[ $mode == "full" ]]; then
		cols=$script_path/anno.cols.full
	elif [[ $mode == "basic" ]]; then
		cols=$script_path/anno.cols.basic.new
	elif [[ $mode == "CNV" ]]; then
		cols=$script_path/anno.cols.CNV
	fi
elif [[ $buildver == "hg38" ]]; then
	if [[ $mode == "full" ]]; then
		cols=$script_path/anno.cols.full.hg38
	elif [[ $mode == "basic" ]]; then
		cols=$script_path/anno.cols.basic.hg38
	# elif [[ $mode == "CNV" ]]; then
		# cols=?
	fi
fi

if [[ $cols != "error" ]]; then
	perl $script_path/vcfGivenInfoToTsv.pl $input_vcf $cols >> $output.tsv

else
	echo "Cols error"
fi




echo "END convertVCF2TSV : ouput > $output.tsv"


