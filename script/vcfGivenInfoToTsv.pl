#!/usr/bin/perl
###################################################################
#########  AUTHOR : Dheeraj Reddy BOBBILI and Patrick MAY #########
###################################################################


use strict;

my $vcf=$ARGV[0];
my $cols=$ARGV[1];

my @cols=();
my $ct=0;

open(COLS,$cols) or die $!;
while(my $str=<COLS>){
	chomp($str);
	next if $str !~ /\S/;
	push @cols,$str;
}
close(COLS);


my $orgformat;
my @orgsamples=();

my $verbose=0;

open(VCF,$vcf) or die $!;
while(my $str=<VCF>){
	chomp($str);
	next if length($str) eq 0;
	if ($str=~/^##/){
	   #print $str."\n";
	next;
	}

	my ($chr,$pos,$id,$ref,$alt,$qual,$filter,$info,$format,@samples)=split(/\t/,$str);
	if($str=~/^#CHROM/){
	$orgformat=$format;
	@orgsamples=@samples;
	print join("\t",@cols)."\tFORMAT\t".join("\t",@orgsamples)."\n";

	next;
	}

	print STDERR "# NEW: $str\n" if $verbose;
	my @infos=split(/;/,$info);
	my %infos=();
	foreach my $infos (@infos){
	print STDERR "# NEWINFO: $infos\n" if $verbose;
		if ($infos=~/\=/){
		my ($key,$value)=split(/=/,$infos) if $infos=~/=/;
		print STDERR "# key=$key value=$value\n" if $verbose;
		#push @cols,$key if $ct eq 0;
		$value =~ s/Name_//g;
		$value =  "NA" if $value eq ".";
		$value =~ s/\\x3d/=/g if $value =~ /x3d/;
		$value =~ s/\\x3b/;/g if $value =~ /x3b/;
		$infos{$key}=$value;
	}else{
		print STDERR "# WARN: INFO $infos not processed\n" if $verbose;
	}
	}
	#print STDERR "# Number of info cols:".scalar(@cols)."\n";

	my $string="$chr\t$pos\t$id\t$ref\t$alt\t$qual\t$filter";

	#for(my $i=7;$i<scalar(@cols)-scalar(@samples)-1;$i++){
	for(my $i=7;$i<scalar(@cols);$i++){
	my $key=$cols[$i];
	if(exists($infos{$key})){
		$string.="\t".$infos{$key};
	}else{
		$string.="\t";
		#print STDERR "# WARN KEY $key does not exist for $str\n";
	}
	}

	$string.="\t$format";
	$string.="\t".join("\t",@samples);

	print "$string\n";
}
close(VCF);