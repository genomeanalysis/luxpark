#!/usr/bin/bash
######################################################
#########  AUTHOR : Sinthuja PACHCHEK PEIRIS #########
######################################################


# ./SamplesCount_by_variants.sh input_tsv


input_tsv=$1

controls="controls.list" 
control_inf_age_50="control_inf_age_50.list"
control_sup_age_50="control_sup_age_50.list"
cases="cases_PD_PDD.list"
cases_alphasyn="cases_alphasyn_PD_PDD_DLB_MSA.list"
cases_tau_pathies="cases_tau_pathies_PSP_CBS_FTDP.list"
cases_Secondary_parkinsonism_vascular_parkinsonism="cases_Secondary_parkinsonism_vascular_parkinsonism.list"
unspecified_disease="/unspecified_disease.list"

touch new_list

FORMAT=$(cat $input_tsv |head -1 | tr '/\t' '\n' | awk '/FORMAT/ {print NR}' | awk '{print $0}')
python ./SamplesCount_by_variants.py $input_tsv Final.$input_tsv $FORMAT $cases $controls $unspecified_disease $control_inf_age_50 $control_sup_age_50 $cases_alphasyn $cases_tau_pathies $cases_Secondary_parkinsonism_vascular_parkinsonism new_list

     

echo "END SamplesCount_by_variants"

