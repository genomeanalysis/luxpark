#!/usr/bin/python

######################################################
#########  AUTHOR : Sinthuja PACHCHEK PEIRIS #########
######################################################


# python ./association_study.py input.tsv continious_outcome.list binary_outcome.list target_column


# GBA_nonGBA
# severe_vs_nonGBA
# mild_vs_nonGBA
# lowrisk_vs_nonGBA
# mild_lowrisk_vs_nonGBA
# severe_mild_vs_nonGBA
# severe_vs_mild_lowrisk
# severe_mild_vs_lowrisk


## adjusted : Gender + Age_at_Basic_Assessment + Disease_duration
## target must be : binary : GBA_carriers : GBA_nonGBA (1) and GBA_nonGBA (0)



import sys
import pandas
import csv
import numpy
import re
from numpy import nan
from numpy import isnan
import os
import pandas as pd
import numpy as np
import statsmodels.api as sm
import statsmodels.formula.api as smf
from statsmodels.stats.multitest import fdrcorrection
import warnings
from statsmodels.sandbox.stats.multicomp import multipletests

warnings.filterwarnings('ignore')


input_tsv= sys.argv[1]
continious_outcome= sys.argv[2]
binary_outcome= sys.argv[3]
target = sys.argv[4]


inputdata = open(input_tsv, "r")
data = pandas.read_csv(inputdata, sep = "\t")

### cleaning data 
gender = {'Male': 1,'Female': 0}
data.Gender = [gender[item] for item in data.Gender]
data = data.replace('Don\'t know', numpy.nan, regex=True)
data = data.replace('NaN', numpy.nan, regex=True)
data = data.replace('not applicable', numpy.nan, regex=True)
data=data.replace(['YES', 'Yes'], True)
data=data.replace(['NO', 'No'], False)
# data = data.replace({False: 0, True: 1})
data = data.replace(",", ".")
data = data[data[target].notna()]


# data.to_csv("input.final.tsv", index=False, sep='\t')

def Logistic_regression(binary_outcome,data,target):

	lenght = len(data)
	binary_regression = []
	binary_regression = pandas.DataFrame(binary_regression)
	binary_regression_file = open("binary_regression.temp.tsv", "a")
	binary = open(binary_outcome, "r")
	binary_array = []

	for line in binary:
		binary_outcome=line.strip()
		binary_array.append(binary_outcome)

	for i in binary_array:
		#### convert
		data[i] = data[i].astype('float32')
		
		# formula='{} ~ C({})'.format(i,target)
		formula='{} ~ C({}) + C(Gender) + Age_at_Basic_Assessment + Disease_duration'.format(i,target)
		model = smf.glm(formula=formula, data=data,missing='drop', family=sm.families.Binomial()).fit()	

		binary_regression = []
		logistic_reg = pandas.DataFrame(binary_regression)

		logistic_reg['β'] = round(model.params,2)
		logistic_reg['std err'] = round(model.bse,2)
		logistic_reg[['95% CI1','95% CI2']]= round(model.conf_int(alpha=0.05, cols=None),2)

		logistic_reg['OR'] = round(numpy.exp(model.params),2)
		logistic_reg[['CI1','CI2']]= round(numpy.exp(model.conf_int(alpha = 0.05)),2)
		logistic_reg['pValues']= round(model.pvalues,4)
		logistic_reg['missign values'] = round(data[i].isnull().sum(),0)
		logistic_reg['% missign values'] = round(((data[i].isnull().sum())*100)/lenght,1)
		logistic_reg['missign values (%)'] = '{} ({}%)'.format(round(data[i].isnull().sum(),0),round(((data[i].isnull().sum())*100)/lenght,1))

		### GBA_carriers
		mask_GBA_carriers = (data[target] == True)
		data_GBA_carriers=data[mask_GBA_carriers]	
		lenght_GBA_carriers = len(data_GBA_carriers)
		mask_GBA_carriers_outcomes = ((data[target] == True) & (data[i] == True))
		data_GBA_carriers_outcomes=data[mask_GBA_carriers_outcomes]	
		lenght_GBA_carriers_outcomes = len(data_GBA_carriers_outcomes)
		logistic_reg['YES'] = round(lenght_GBA_carriers_outcomes,0)
		logistic_reg['YES %'] = round(lenght_GBA_carriers_outcomes *100  / lenght_GBA_carriers,1)
		logistic_reg['YES (%)'] = '{} ({}%)'.format(lenght_GBA_carriers_outcomes,round(lenght_GBA_carriers_outcomes *100  / lenght_GBA_carriers,1))

		## non_GBA_carriers
		mask_non_GBA_carriers = (data[target] == False)
		data_non_carriers=data[mask_non_GBA_carriers]
		lenght_non_carriers = len(data_non_carriers)
		mask_non_GBA_carriers_outcomes = ((data[target] == False) & (data[i] == True))
		non_data_GBA_carriers_outcomes=data[mask_non_GBA_carriers_outcomes]	
		lenght_non_GBA_carriers_outcomes = len(non_data_GBA_carriers_outcomes)
		logistic_reg['NO'] = round(lenght_non_GBA_carriers_outcomes,0)
		logistic_reg['NO %'] = round(lenght_non_GBA_carriers_outcomes *100  / lenght_non_carriers,1)
		logistic_reg['NO (%)'] = '{} ({}%)'.format(lenght_non_GBA_carriers_outcomes,round(lenght_non_GBA_carriers_outcomes *100  / lenght_non_carriers,1))

		### mean/SD
		logistic_reg['Mean Yes']= "-"
		logistic_reg['SD Yes']= "-"
		logistic_reg['Mean No']= "-"
		logistic_reg['SD No']= "-"		
		logistic_reg['Model']= "Logistic"

		# print(logistic_reg)

		### set index for output
		logistic_reg = logistic_reg.reset_index()
		logistic_reg.loc[logistic_reg['index'].str.contains(target), 'index'] = i
		binary_regression = logistic_reg[logistic_reg['index'].str.contains(i)]
		binary_regression.to_csv(binary_regression_file, index=False, sep='\t')


def Lineaire_regression(continious_outcome,data,target):
	lenght = len(data)
	lineaire_regression = []
	lineaire_regression = pandas.DataFrame(lineaire_regression)
	lineaire_regression_file = open("lineaire_regression.temp.tsv", "a")
	continious = open(continious_outcome, "r")
	continious_array = []

	for line in continious:
		continious_outcome=line.strip()
		continious_array.append(continious_outcome)

	for i in continious_array:
		data[i] = data[i].astype('float32')

		formula='{} ~ C({}) + C(Gender) + Age_at_Basic_Assessment + Disease_duration'.format(i,target)
		# # formula='{} ~ C({})'.format(i,target)
		model = smf.glm(formula=formula, data=data,missing='drop').fit()

		# print(model.summary())
		lineaire_regression = []
		lin_reg_odds = pandas.DataFrame(lineaire_regression)

		lin_reg_odds['β'] = round(model.params,2)
		lin_reg_odds['std err'] = round(model.bse,2)
		lin_reg_odds[['95% CI1','95% CI2']]= round(model.conf_int(alpha=0.05, cols=None),3)

		lin_reg_odds['OR'] = round(numpy.exp(model.params),2)
		lin_reg_odds[['CI1','CI2']]= round(numpy.exp(model.conf_int(alpha = 0.05)),2)
		lin_reg_odds['pValues']= round(model.pvalues,4)
		lin_reg_odds['missign values'] = round(data[i].isnull().sum(),0)
		lin_reg_odds['% missign values'] = round(((data[i].isnull().sum())*100)/lenght,1)
		lin_reg_odds['missign values (%)'] = '{} ({}%)'.format(round(data[i].isnull().sum(),0),round(((data[i].isnull().sum())*100)/lenght,1))	

		
		### GBA_carriers
		mask_GBA_carriers = (data[target] == True)
		data_GBA_carriers=data[mask_GBA_carriers]	
		lenght_GBA_carriers = len(data_GBA_carriers)
		GBA_carriers = data_GBA_carriers[i].notnull().sum()
		lin_reg_odds['YES'] = GBA_carriers
		lin_reg_odds['YES %'] = round(GBA_carriers *100  / lenght_GBA_carriers,1)
		lin_reg_odds['YES (%)'] = '{} ({}%)'.format(GBA_carriers,round(GBA_carriers *100  / lenght_GBA_carriers,1))

		## non_GBA_carriers
		mask_non_GBA_carriers = (data[target] == False)
		data_non_GBA_carriers=data[mask_non_GBA_carriers]	
		lenght_non_GBA_carriers = len(data_non_GBA_carriers)
		non_GBA_carriers = data_non_GBA_carriers[i].notnull().sum()
		lin_reg_odds['NO'] = non_GBA_carriers
		lin_reg_odds['NO %'] = round(non_GBA_carriers *100  / lenght_non_GBA_carriers,1)
		lin_reg_odds['NO (%)'] = '{} ({}%)'.format(non_GBA_carriers,round(non_GBA_carriers *100  / lenght_non_GBA_carriers,1))

	 	### mean/SD
		lin_reg_odds['Mean Yes']= round(data_GBA_carriers[i].mean(skipna = True),1)
		lin_reg_odds['SD Yes']= round(data_GBA_carriers[i].std(skipna = True),1)
		lin_reg_odds['Mean No']= round(data_non_GBA_carriers[i].mean(skipna = True),1)
		lin_reg_odds['SD No']= round(data_non_GBA_carriers[i].std(skipna = True),1)		
		lin_reg_odds['Model']= "Lineaire"
		lin_reg_odds['YES Mean (±SD)'] = '{} (±{})'.format(round(data_GBA_carriers[i].mean(skipna = True),1),round(data_GBA_carriers[i].std(skipna = True),1))
		lin_reg_odds['NO Mean (±SD)'] = '{} (±{})'.format(round(data_non_GBA_carriers[i].mean(skipna = True),1),round(data_non_GBA_carriers[i].std(skipna = True),1))


		# print(lin_reg_odds)

		### set index for output
		lin_reg_odds = lin_reg_odds.reset_index()
		lin_reg_odds.loc[lin_reg_odds['index'].str.contains(target), 'index'] = i
		lineaire_regression = lin_reg_odds[lin_reg_odds['index'].str.contains(i)]
		lineaire_regression.to_csv(lineaire_regression_file, index=False, sep='\t')

def clean_temp(file, output):
	inputfile = open(file, "r")
	data = pandas.read_csv(inputfile, sep = "\t")
	# # remove duplicate rows
	data = data[data["index"].str.contains("index") == False]
	data = data.drop_duplicates()
	data.rename(columns={'index': 'Clinical characteristics and scales'}, inplace=True)
	os.remove(file)
	data.to_csv(output, index=False, sep='\t')

def FDRcorrection(lineaire_regression, binary_regression, output):
	data_lineaire_regression = pandas.read_csv(lineaire_regression, delimiter='\t')
	data_binary_regression = pandas.read_csv(binary_regression, delimiter='\t')
	data_outer = pandas.concat([data_lineaire_regression, data_binary_regression], ignore_index=True)
	data_outer2 = data_outer.replace(regex=r'nan', value='')
	data_outer2['pValues'] = data_outer2['pValues'].astype('float32')
	data_outer2['adj_pValue_fdr']=fdrcorrection(pvals=data_outer2['pValues'], alpha=0.05, method='indep', is_sorted=False)[1]
	data_outer2['adj_pValue_fdr']=round(data_outer2['adj_pValue_fdr'],4)
	data_outer2['adj_pValue_Bonferroni'] = multipletests(pvals=data_outer2['pValues'], alpha=.05, method='bonferroni')[1]
	data_outer2['adj_pValue_Bonferroni'] = round(data_outer2['adj_pValue_Bonferroni'],4)

	data_outer2 = data_outer2[['Clinical characteristics and scales','β','std err','95% CI1','95% CI2','Mean Yes','SD Yes','YES Mean (±SD)','Mean No','SD No','NO Mean (±SD)','YES','YES %','YES (%)','NO','NO %','NO (%)','missign values','% missign values','missign values (%)','OR','CI1','CI2','pValues','adj_pValue_fdr','adj_pValue_Bonferroni','Model']]

	### sort
	sorter = ['AAO','AAO < 45','AAO > 45','AAO_bin','Family_History_of_Parkinsons_Disease','Family_History_of_Dementia','Family_History_of_Tremor','Hoehn_and_Yahr_Staging','MDS_UPDRS_Total_Score_Part_II','MDS_UPDRS_Total_Score_Part_III','MDS_UPDRS_Total_Score_Part_IV','Current_Motor_Symptom_Dyskinesias','Current_Motor_Symptom_Falls','Current_Motor_Symptom_Freezing','Current_Motor_Symptom_Gait_Disorder','Current_Motor_Symptom_Motor_fluctuation','BDI_Total_Score','FAQ_Score','MDS_UPDRS_Total_Score_Part_I','NMS_Total_Score','PDSS_Total_Score','SCOPA_Total_Score','Sniff_Total_Score','SPARK_Total_Score','Total_Points_for_MOCA','Current_non_motor_symptom_Compulsions_control_disorder','Current_non_motor_symptom_Constipation','Current_non_motor_symptom_Dysphagia','Current_non_motor_symptom_Excessive_daytime_sleepiness_NOS','Current_non_motor_symptom_Insomnia_NOS','Current_non_motor_symptom_Orthostatism','Current_non_motor_symptom_Psychosis','Current_non_motor_symptom_Urinary_incontinence','REM_Score_bi','Levodopa_Equivalent_Daily_Dose_LEDD','PDQ39_Total_Score_points','Medical_History_Cardiovascular_Cardiovascular_disease','Medical_History_Cardiovascular_Hypertension','Medical_History_Metabolic_Diabetes','Medical_History_Metabolic_Hypercholesterolemia','Medical_History_Neurological_Traumatic_Brain_Injury']

	data_outer2 = data_outer2.set_index('Clinical characteristics and scales')
	data_outer2 = data_outer2.reindex(sorter)

	data_outer2.to_csv(output, index=True, sep='\t')
	os.remove(lineaire_regression)
	os.remove(binary_regression)



# ###### RUN 
Logistic_regression(binary_outcome,data,target)
binary_regression = "binary_regression.temp.tsv"
output_regression = "binary_regression.tsv"
clean_temp(binary_regression,output_regression)

Lineaire_regression(continious_outcome,data,target)
lineaire_regression = "lineaire_regression.temp.tsv"
output_regression = "lineaire_regression.tsv"
clean_temp(lineaire_regression,output_regression)

lineaire_regression = "lineaire_regression.tsv"
binary_regression = "binary_regression.tsv"
output = "{}.regression.tsv".format(target)
FDRcorrection(lineaire_regression, binary_regression, output)



