#!/usr/bin/python
####################################################
#########  AUTHOR : Dheeraj Reddy BOBBILI  #########
####################################################


from __future__ import division
import os,sys,gzip
#Usage python change_genotype.py <vcf>
#This script is used to change the genotype of multi allelic vcf files

vcf=sys.argv[1]

if vcf.endswith("gz"):
        infile=gzip.open(vcf, "r")
else:
        infile=open(vcf, "r")

for a in infile:
	if a.startswith("#"):
		print a.rstrip()
	else:
		a=a.rstrip().split("\t")
		print "\t".join(a[0:9])+"\t"+"\t".join([c.split(":")[0].replace(".", "1")+":"+str(c.split(":")[1].split(",")[0])+","+str(sum(map(int, c.split(":")[1].split(",")[1:])))+":"+":".join(c.split(":")[2:]) if len(c.split(":")[1].split(","))>2 else c for c in a[9:]])

